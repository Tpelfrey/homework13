package Homework13;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


/**
 * Created by Tim McGowen on 3/22/2017
 *
 * This is the controller for the FXML document that contains the view. 
 */
public class WxController implements Initializable {

  @FXML
  private Button btngo;

  @FXML
  private TextField txtzipcode;

  @FXML
  private Label lblTime;

  @FXML
  private Label lblFull;

  @FXML
  private Label lblWeather;

  @FXML
  private Label lblTemp_f;

  @FXML
  private Label lblWind_String;

  @FXML
  private Label lblPressure_In;

  @FXML
  private Label lblVisibility_Mi;

  @FXML
  private ImageView iconwx;

  @FXML
  private void handleButtonAction(ActionEvent e) {
    // Create object to access the Model
    WxModel weather = new WxModel();

    // Has the go button been pressed?
    if (e.getSource() == btngo)
    {
    	//System.out.println("Button Pushed");
      String zipcode = txtzipcode.getText();
      if (weather.getWx(zipcode))
      {
    	  //System.out.println(" Zip Obtained");
        lblFull.setText(weather.getLocation());
        lblTime.setText(weather.getTime());
        lblWeather.setText(weather.getWeather());
        lblTemp_f.setText(String.valueOf(weather.getTemp()));
        lblWind_String.setText(weather.getWind());
        lblPressure_In.setText(weather.getPressure());
        lblVisibility_Mi.setText(weather.getVisibility_Mi());
        iconwx.setImage(weather.getImage());
      }
      else
      {
    	  //System.out.println("No Zip");
        lblFull.setText("Invalid Zipcode");
        lblTime.setText("");
        lblWeather.setText("");
        lblTemp_f.setText("");
        lblWind_String.setText("");
        lblPressure_In.setText("");
        lblVisibility_Mi.setText("");
        
        iconwx.setImage(new javafx.scene.image.Image(getClass().getResource("badzipcode.png").toExternalForm()));
      }
    }
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {
    // TODO
  }    

}
