package Homework13;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javafx.scene.image.Image;

/**
 * Created by Tim McGowen on 3/22/2017
 *
 * Model to get weather information based on zipcode.
 */
public class WxModel {
  private JsonElement jse;
  private final String apiKey = "3ad71c9a89c72c3c";

  public boolean getWx(String zip)
  {
    try
    {
      URL wuURL = new URL("http://api.wunderground.com/api/" + apiKey +
          "/conditions/q/" + zip + ".json");

      // Open connection
      InputStream is = wuURL.openStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));

      // Read the results into a JSON Element
      jse = new JsonParser().parse(br);

      // Close connection
      is.close();
      br.close();
    }
    catch (java.io.UnsupportedEncodingException uee)
    {
    }
    catch (java.net.MalformedURLException mue)
    {
    }
    catch (java.io.IOException ioe)
    {
    }
    catch (java.lang.NullPointerException npe)
    {
    }

    // Check to see if the zip code was valid.
    return isValid();
  }

  public boolean isValid()
  {
    // If the zip is not valid we will get an error field in the JSON
    try {
      String error = jse.getAsJsonObject().get("response").getAsJsonObject().get("error").getAsJsonObject().get("description").getAsString();
      return false;
    }

    catch (java.lang.NullPointerException npe)
    {
      // We did not see error so this is a valid zip
      return true;
    }
  }
  
  public String getLocation()
  {
	  //System.out.println("Location Working");
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").
    		getAsJsonObject().get("full").getAsString();
  }
  
  public String getTime()
  {
	  //System.out.println("Time Working");
	  Calendar cal = Calendar.getInstance();
		String strTimeConfig = new SimpleDateFormat("MMMMMMMMM dd, hh:mm a z").format(cal.getTime());
		String strTime = "Last Updated on " + strTimeConfig;
		return strTime;
  }
  
  public String getWeather() 
  {
	 // System.out.println("Weather Working");
	return jse.getAsJsonObject().get("current_observation").
		getAsJsonObject().get("weather").getAsString();  
  }

  public double getTemp()
  {
	  //System.out.println("Temperature Working");
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").
    		getAsDouble();
  }

  public String getWind()
  {
	  //System.out.println("Wind Working");
	 return jse.getAsJsonObject().get("current_observation").
		getAsJsonObject().get("wind_string").getAsString();
  }
  
  public String getPressure()
  {
	  //System.out.println("Pressure Working");
	 return jse.getAsJsonObject().get("current_observation").
		getAsJsonObject().get("pressure_in").getAsString();
  }
  
  public String getVisibility_Mi()
  {
	 // System.out.println("Visibility Working");
	return jse.getAsJsonObject().get("current_observation")
			.getAsJsonObject().get("visibility_mi").getAsString();
	  
  }
  
  public Image getImage()
  {
	  //System.out.println("Image Working");
    String iconURL = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon_url").getAsString();
    return new Image(iconURL);
  }
}
